package com.test.vastexoplayer;


import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.ads.interactivemedia.v3.api.AdDisplayContainer;
import com.google.ads.interactivemedia.v3.api.AdErrorEvent;
import com.google.ads.interactivemedia.v3.api.AdEvent;
import com.google.ads.interactivemedia.v3.api.AdsLoader;
import com.google.ads.interactivemedia.v3.api.AdsManager;
import com.google.ads.interactivemedia.v3.api.AdsManagerLoadedEvent;
import com.google.ads.interactivemedia.v3.api.AdsRequest;
import com.google.ads.interactivemedia.v3.api.ImaSdkFactory;
import com.google.ads.interactivemedia.v3.api.ImaSdkSettings;
import com.google.ads.interactivemedia.v3.api.player.ContentProgressProvider;
import com.google.ads.interactivemedia.v3.api.player.VideoProgressUpdate;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.analytics.AnalyticsListener;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.video.VideoListener;


import java.util.ArrayList;

import ir.tapsell.sdk.Tapsell;


public class VastActivity extends AppCompatActivity implements View.OnClickListener,
        AdEvent.AdEventListener, AdErrorEvent.AdErrorListener {

    private static final String TAG = "VASTActivity";
    // Whether an ad is displayed.
    private boolean isAdDisplayed;

    // The video player.
    private VideoView videoView;
    private TextView tvLog;
    // The play button to trigger the ad request.

    // The container for the ad's UI.
    private ViewGroup adUiContainer;

    // Factory class for creating SDK objects.
    private ImaSdkFactory imaSdkFactory;

    // The AdsLoader instance exposes the requestAds method.
    private AdsLoader adsLoader;

    // AdsManager exposes methods to control ad playback and listen to ad events.
    private AdsManager adsManager;

    private SimpleExoPlayer player;
    private PlayerView playerView;
    private Button btnRequest;

    private String firstLink = "https://api.tapsell.ir/v2/pre-roll/5cac31e2b4ace90001836d64/vast?device-model=SM-G950F&screenWidth=1440&device-os=android&appVersionCode=8&osVersion=28&screenOrientation=1&dev-platform=Android&t-user-id=&deviceLanguage=English&device-imei=&deviceManufacturer=samsung&networkType=13&npa=false&osAdvertisingId=16aeabd1-279b-4474-9d7d-99c9f443eab8&secretKey=kttjtpmdehsmnhlkkrlfekisnfifqtdallotfeccaspodsnqspelhcinjjdbiqtmhaglsn&screenHeight=2768&osId=eb0081bfb9c28fe8&appVersionName=1.3&app-package-name=ir.tapsell.sample.android&limitAdTrackingEnabled=false&carrier=Irancell&device-client-date=1585486395263&customer-user-id=&sdkVersion=4.5.0&screenDensity=4.0&dataAvailability=false&targetPlatform=1&deviceBrand=samsung";
    private String secondLink = "https://api.tapsell.ir/v2/pre-roll/5e804ecf0f2eb80001aa77cd/vast?secretKey=sqpsmereqnlgojlpdaojjbpirqpjkagedfabbsddpeflthslkhoqdlnpqgleogdjrkpjrr&targetPlatform=1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vast);

        btnRequest = findViewById(R.id.btnRequest);
        btnRequest.setOnClickListener(this);
        playerView = findViewById(R.id.exoPlayer);


        videoView = findViewById(R.id.videoPlayer);
        adUiContainer = findViewById(R.id.videoPlayerWithAdPlayback);
        tvLog = findViewById(R.id.tvLog);

        // Create an AdsLoader.
        imaSdkFactory = ImaSdkFactory.getInstance();
        AdDisplayContainer adDisplayContainer =
                ImaSdkFactory.createAdDisplayContainer(
                        adUiContainer,
                        ImaSdkFactory.createSdkOwnedPlayer(this, adUiContainer));

        ImaSdkSettings settings = imaSdkFactory.createImaSdkSettings();
        settings.setLanguage("fa");
        adsLoader = imaSdkFactory.createAdsLoader(this, settings, adDisplayContainer);

        // Add listeners for when ads are loaded and for errors.
        adsLoader.addAdErrorListener(this);
        adsLoader.addAdsLoadedListener(
                new AdsLoader.AdsLoadedListener() {
                    @Override
                    public void onAdsManagerLoaded(AdsManagerLoadedEvent adsManagerLoadedEvent) {
                        // Ads were successfully loaded, so get the AdsManager instance.
                        // AdsManager has events for ad playback and errors.
                        adsManager = adsManagerLoadedEvent.getAdsManager();

                        // Attach event and error event listeners.
                        adsManager.addAdErrorListener(VastActivity.this);
                        adsManager.addAdEventListener(VastActivity.this);
                        adsManager.init();
                    }
                });

        // Add listener for when the content video finishes.
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.reset();
                mediaPlayer.setDisplay(videoView.getHolder());

                if (adsLoader != null) {
                    adsLoader.contentComplete();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        videoView.setVideoPath(
                "https://storage.backtory.com/tapsell-server/sdk/VASTContentVideo.mp4");
        requestAds(firstLink);
    }


    private void requestAds(String adTagUrl) {
        // Create the ads request.
        AdsRequest request = imaSdkFactory.createAdsRequest();
        request.setAdTagUrl(adTagUrl);
        request.setContentProgressProvider(
                new ContentProgressProvider() {
                    @Override
                    public VideoProgressUpdate getContentProgress() {
                        if (isAdDisplayed || videoView == null || videoView.getDuration() <= 0) {
                            return VideoProgressUpdate.VIDEO_TIME_NOT_READY;
                        }
                        return new VideoProgressUpdate(
                                videoView.getCurrentPosition(), videoView.getDuration());
                    }
                });

        // Request the ad. After the ad is loaded, onAdsManagerLoaded() will be called.
        adsLoader.requestAds(request);
    }

    @Override
    public void onAdEvent(AdEvent adEvent) {
        Log.i(TAG, "Event: " + adEvent.getType());

        tvLog.append(adEvent.getType().name() + "\n");
        // These are the suggested event types to handle. For full list of all ad event
        // types, see the documentation for AdEvent.AdEventType.
        switch (adEvent.getType()) {
            case LOADED:
                // AdEventType.LOADED will be fired when ads are ready to be played.
                // AdsManager.start() begins ad playback. This method is ignored for VMAP or
                // ad rules playlists, as the SDK will automatically start executing the
                // playlist.
                adsManager.start();
                hideBtnAndShowVastWhenIsReady();
                break;

            case CONTENT_PAUSE_REQUESTED:
                // AdEventType.CONTENT_PAUSE_REQUESTED is fired immediately before a video
                // ad is played.
                isAdDisplayed = true;
                videoView.pause();
                break;

            case CONTENT_RESUME_REQUESTED:
                // AdEventType.CONTENT_RESUME_REQUESTED is fired when the ad is completed
                // and you should start playing your content.
                isAdDisplayed = false;
                videoView.start();
                break;

            case ALL_ADS_COMPLETED:
                if (adsManager != null) {
                    videoView.pause();
                    hideVastAndShowMp4();
                    initializePlayer();
                    adsManager.destroy();
                    adsManager = null;
                }
                break;

            case SKIPPED:
                Log.i(TAG, " VAST SKIPPED => ");
//                videoView.pause();
//                adsManager.destroy();
//                adsManager = null;
//                hideVastAndShowMp4();
//                initializePlayer();
            case CLICKED:
                Log.i(TAG, "CLICKED");
                adsManager.getAdCuePoints();

            case TAPPED:
                Log.i(TAG, "TAPED");
            default:
                break;
        }
    }

    @Override
    public void onAdError(AdErrorEvent adErrorEvent) {
        tvLog.append(adErrorEvent.getError().getMessage() + "\n");
        Log.e(TAG, "Ad Error: " + adErrorEvent.getError().getMessage());
        videoView.start();
        videoView.pause();
        initializePlayer();
        hideVastAndShowMp4();
    }

    @Override
    public void onResume() {
        if (adsManager != null && isAdDisplayed) {
            adsManager.resume();
        } else {
            videoView.start();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if (adsManager != null && isAdDisplayed) {
            adsManager.pause();
        } else {
            videoView.pause();
        }
        if (player != null) {
            player.release();
        }
        super.onPause();
    }

    private void initializePlayer() {
        if (player == null) {
            player = new SimpleExoPlayer.Builder(this).build();

            playerView.setPlayer(player);
            player.setPlayWhenReady(true);

        }

        ArrayList uriArrayList = new ArrayList<Uri>();
        uriArrayList.add(Uri.parse("https://as11.cdn.asset.aparat.com/aparat-video/171c63d42c1a8efda10fee1dd02b2b3820582790-144p.mp4"));
        uriArrayList.add(Uri.parse("https://as7.cdn.asset.aparat.com/aparat-video/1c9e95ee3c2c09949c23891428f84c2620514124-144p.mp4"));
        MediaSource mediaSource = buildMediaSource(uriArrayList);

        player.prepare(mediaSource, true, true);

        player.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playWhenReady){
                    Log.i(TAG, "if player was Ready => onPlayerStateChanged =>" + playbackState);
                    player.stop();
                } else {
                    Log.e(TAG, "onPlayerStateChanged =>" + playbackState);
                }
            }




        });

        player.hasNext();

        player.addAnalyticsListener(new AnalyticsListener() {
        });

        player.addVideoListener(new VideoListener() {
        });
    }

    private ConcatenatingMediaSource buildMediaSource(ArrayList<Uri> uriArrayList) {
        ConcatenatingMediaSource concatenatingMediaSource = new ConcatenatingMediaSource();
        for (Uri uri : uriArrayList){
            concatenatingMediaSource.addMediaSource(new ExtractorMediaSource.Factory(new DefaultHttpDataSourceFactory("app")).createMediaSource(uri));
        }

        return concatenatingMediaSource;
    }


    private void hideVastAndShowMp4() {
        btnRequest.setVisibility(View.GONE);
        adUiContainer.setVisibility(View.GONE);
        playerView.setVisibility(View.VISIBLE);

    }

    private void hideBtnAndShowVastWhenIsReady() {
        btnRequest.setVisibility(View.GONE);
        adUiContainer.setVisibility(View.VISIBLE);
        videoView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}